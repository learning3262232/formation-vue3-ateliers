import {defineStore} from 'pinia'
import { computed, ref } from 'vue';
import CalculatorService from '@/service/CalculatorService';

export const useCalculatorStore = defineStore('Calculator',()  =>{
    const param1 = ref<number>(0)
    const param2 = ref<number>(0)
    const message = ref<string>('')
    const result = ref<number>(0)

    const isResultat = computed(() => result.value>0)
    const msg = computed(() => message.value + result.value)
    function sum(): void{
        message.value = 'Resultat Addition de: '+param1.value + ' & '+param2.value+ ' donne: '
        result.value = CalculatorService.sum(param1.value, param2.value)
    }
    function substract(): void {
        message.value = 'Resultat Soustraction de: '+param1.value + ' & '+param2.value+ ' donne: '
        result.value = CalculatorService.substract(param1.value, param2.value)
    }
    function multiplication(): void {
        message.value = 'Resultat Multiplication de: '+param1.value + ' & '+param2.value+ ' donne: '
        result.value = CalculatorService.multiplication(param1.value, param2.value)
    }
    function modulo(): void {
        message.value = 'Resultat Modulo de: '+param1.value + ' & '+param2.value+ ' donne: '
        result.value = CalculatorService.modulo(param1.value, param2.value)
    }

    function clear() : void {
        param1.value = 0
        param2.value = 0
        message.value = ''
        result.value = 0
    }

    return {sum, substract, multiplication, modulo,clear, param1, param2, msg, isResultat}
})