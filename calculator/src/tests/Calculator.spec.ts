import {describe, it, expect } from 'vitest'
import CalculatorService from '@/service/CalculatorService'

describe('calculate', () =>{
    it('get Sum of 2 numbers', () =>{
        expect(CalculatorService.sum(3, 5)).toBe(8);
    })

    it('get modulo of 2 numbers', () => {
        expect(CalculatorService.modulo(7, 2)).toBe(1)
    })
    it('get multiplication of 2 numbers', () =>{
        expect(CalculatorService.multiplication(4, 5)).toBeGreaterThan(15)
    })

    it('get substraction of 2 numbers', () => {
        expect(CalculatorService.substract(5, 3)).toBe(2)
    })
})