describe('Calculate operations', () => {
  it('when click sum button then sum 2 numbers', () => {
    cy.visit('/')
    cy.get('.param1').type(5)
    cy.get('.param2').type(3)
    cy.get('.sumButton').click()
    cy.get('.resultat').should('be.visible')
    setTimeout(() => {
      cy.get('.resultat').should('contains.value', 'Resultat Addition de: 5 & 3 donne: 8')
    }, 5000);

  })

  it('when click modulo button then modulo of 2 numbers', () => {
    cy.visit('/')
    cy.get('.param1').type(5)
    cy.get('.param2').type(3)
    cy.get('.moduloButton').click()
    cy.get('.resultat').should('be.visible')
    setTimeout(() => {
      cy.get('.resultat').should('contains.value', 'Resultat Modulo de: 5 & 3 donne: 2')
    }, 5000);
    

  })

  it('when click substract button then substraction of 2 numbers', () => {
    cy.visit('/')
    cy.get('.param1').type(8)
    cy.get('.param2').type(4)
    cy.get('.substractButton').click()
    cy.get('.resultat').should('be.visible')
    setTimeout(() => {
      cy.get('.resultat').should('contains.value', 'Resultat Modulo de: 8 & 4 donne: 4')
    }, 5000);
    

  })
})